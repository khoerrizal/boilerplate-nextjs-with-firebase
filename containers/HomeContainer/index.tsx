import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";
import LoginForm from "../../components/molecules/LoginForm"
import { useGlobalContext } from "../../providers/globalProvider";

const HomeContainer: React.FC = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['user']);
    const router = useRouter();
    const {
        authState: { user, logOut }
    } = useGlobalContext()

    const handleLogout = async () => {
        try {
            await logOut();
            removeCookie('user')
            router.push("/login");
        } catch (error: any) {
            console.log(error.message);
        }
    };

    return (
        <div>
            {!user.uid ? (
                <LoginForm/>
            ):(
                <>
                    <h1>Wellcome!!!!!</h1>
                    <span onClick={handleLogout} className="cursor-pointer">Logout</span>
                </>
            )}
        </div>
    )
}

export default HomeContainer