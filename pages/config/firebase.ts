// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCGlXhn9O_nSvXitPkjMRKtoRVPkLYL8Jc",
  authDomain: "note-app-rizal.firebaseapp.com",
  projectId: "note-app-rizal",
  storageBucket: "note-app-rizal.appspot.com",
  messagingSenderId: "1000918681934",
  appId: "1:1000918681934:web:a00361aee5422c119b361f",
  measurementId: "G-8SF0EXS2R1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth();