import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { GlobalProvider } from '../providers/globalProvider'
import { CookiesProvider } from "react-cookie"

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <CookiesProvider>
            <GlobalProvider>
                <Component {...pageProps} />
            </GlobalProvider>
        </CookiesProvider>
    )
}

export default MyApp
