import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import LoginContainer from '../containers/LoginContainer'
import { HomeProvider } from '../providers/homeProvider'

const Login: NextPage = () => {
    const headList = {
        title: 'Login | Koperasi Nusantara',
        description: 'This login page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <LoginContainer/>
            </AppLayout>
        </HomeProvider>
    )
}

export default Login
