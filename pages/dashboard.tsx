import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import DashboardContainer from '../containers/DashboardContainer'
import HomeContainer from '../containers/HomeContainer'
import { parseCookies } from '../helpers'
import { HomeProvider } from '../providers/homeProvider'

const Home: NextPage = () => {
    const headList = {
        title: 'Dashboard | Koperasi Nusantara',
        description: 'This dashbaord page',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <DashboardContainer />
            </AppLayout>
        </HomeProvider>
    )
}

export default Home