import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from 'firebase/auth'
import { createContext, useContext, useEffect, useState } from 'react'
import { useCookies } from 'react-cookie'
import { auth } from '../../pages/config/firebase'
import {
    IAuthState,
    IGlobalState, INotificationState, IProviderProps, UserType
} from './types'

export function createCtx<IGlobalState>() {
    const ctx = createContext<IGlobalState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useGlobalContext, CtxProvider] = createCtx<IGlobalState>()

export const GlobalProvider: React.FC<IProviderProps> = ({ children }) => {
    const [successMsg, setSuccessMsg] = useState<string>('tes success')
    const [errorMsg, setErrorMsg] = useState<string>('')
    const [user, setUser] = useState<UserType>({ email: null, uid: null });
    const [loading, setLoading] = useState<boolean>(true);
    const [cookies, setCookie, removeCookie] = useCookies(['user']);

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setUser({
                email: user.email,
                uid: user.uid,
                });
            } else {
                setUser({ email: null, uid: null });
            }
        });
        setLoading(false);
        if (cookies && cookies.user){
            setUser(cookies.user)
        }

        return () => unsubscribe();
    }, []);

    const signUp = (email: string, password: string) => {
        return createUserWithEmailAndPassword(auth, email, password);
    };

    const logIn = (email: string, password: string) => {
        return signInWithEmailAndPassword(auth, email, password);
    };

    const logOut = async () => {
        setUser({ email: null, uid: null });
        await signOut(auth);
    };
    
    const notificationState: INotificationState = {
        successMsg,
        errorMsg,
        setErrorMsg,
    }

    const authState: IAuthState = {
        user: user,
        signUp: signUp,
        logIn: logIn,
        logOut: logOut,
    }

    const state: IGlobalState = {
        notificationState,
        authState
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}