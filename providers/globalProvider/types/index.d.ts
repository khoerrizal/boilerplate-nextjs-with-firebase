export interface IProviderProps {
    children: JSX.Element
}

export interface UserType {
    email: string | null;
    uid: string | null;
}

export interface INotificationState {
    successMsg: string
    errorMsg: string
    setErrorMsg: (msg: string) => void
}

export interface IAuthState {
    user: UserType
    signUp: (email: string, password: string) => any
    logIn: (email: string, password: string) => any
    logOut: async
}

export interface IGlobalState {
    notificationState: INotificationState
    authState: IAuthState
}