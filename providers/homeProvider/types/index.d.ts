export interface INotificationState {
    successMsg: string
    errorMsg: string
}

export interface IContentState {
    
}

export interface IHomeState {
    notificationState: INotificationState,
    contentState: IContentState
}
