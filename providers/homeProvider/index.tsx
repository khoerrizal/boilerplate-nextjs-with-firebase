import { useState } from 'react'
import { createCtx } from '../globalProvider'
import { IProviderProps } from '../globalProvider/types'
import { IContentState, IHomeState, INotificationState } from './types'

export const [useHomeContext, CtxProvider] = createCtx<IHomeState>()

export const HomeProvider: React.FC<IProviderProps> = ({ children }) => {
    const [successMsg, setSuccessMsg] = useState<string>('tes success')
    const [errorMsg, setErrorMsg] = useState<string>('')

    const notificationState: INotificationState = {
        successMsg,
        errorMsg,
    }

    const contentState: IContentState = {}

    const state: IHomeState = {
        notificationState,
        contentState,
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}
