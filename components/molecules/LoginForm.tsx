import Image from "next/image";
import { useRouter } from "next/router";
import { useState } from "react";
import { useCookies } from "react-cookie";
import { useGlobalContext } from "../../providers/globalProvider";
import { backgroundLogin, buttonMask } from "../../public/images/LoginPage";

const LoginForm: React.FC = () => {
    const [inputEmail, setInputEmail] = useState<string>("")
    const [inputPasword, setInputPassword] = useState<string>("")
    const [cookies, setCookie, removeCookie] = useCookies(['user']);
    const router = useRouter();
    const {
        authState: {
            logIn
        }
    } = useGlobalContext()

    const submitLogin = async (event:any) => {
        event.preventDefault()
        try {
            const res = await logIn(inputEmail, inputPasword);
            console.log(res, "response logingggggggggg")
            setCookie("user", JSON.stringify(res.user), {
                path: "/",
                maxAge: 3600, // Expires after 1hr
                sameSite: true,
            })
            router.push("/dashboard");
        } catch (error: any) {
            console.log(error.message);
        }
    }

    const handleChangeEmail = (event: any) => {
        setInputEmail(event.target.value)
    }

    const handleChangePassword = (event: any) => {
        setInputPassword(event.target.value)
    }

    return (
        <div className="px-8 py-16 w-[375px] relative h-[812px] items-end">
            <Image src={backgroundLogin} alt={"Background Login"} className="absolute left-0 -top-10"/>
            <form onSubmit={submitLogin} className="flex flex-col justify-end h-full gap-6">
                <div className="flex flex-col">
                    <label className="text-[#B9B9B9] font-normal text-sm">Email</label>
                    <input onChange={handleChangeEmail} className="border-[#2743FD] border-b" name="field_email"/>
                </div>
                <div className="flex flex-col">
                    <label className="text-[#B9B9B9] font-normal text-sm">Password</label>
                    <input onChange={handleChangePassword} className="border-[#2743FD] border-b" name="field_password" type="password"/>
                </div>
                <span className="text-[#2B47FC]">Lupa Password?</span>
                <button className="relative font-normal text-xl bg-gradient-to-r from-[#4960F9] to-[#1433FF] text-white p-6 text-left rounded-[28px]" type="submit">
                    <span>Masuk</span>
                    <Image src={buttonMask} alt={"Mask"} className="absolute top-0 right-0 w-full h-20"/>
                </button>
            </form>
        </div>
    )
}

export default LoginForm