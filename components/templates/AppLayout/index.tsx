import Head from 'next/head'
import Footer from '../../molecules/Footer'

export interface AppLayoutProps {
    title: string
    description: string
    children: React.ReactNode
}

const AppLayout: React.FC<AppLayoutProps> = ({ title, description, children }) => {
    return (
        <div className="flex flex-col">
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
            </Head>

            <main className="min-h-screen max-w-[1366px] w-full flex justify-center mx-auto overflow-hidden">
                {children}
            </main>

            <Footer />
        </div>
    )
}

export default AppLayout
